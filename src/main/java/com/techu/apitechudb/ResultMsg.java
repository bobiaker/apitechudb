package com.techu.apitechudb;

import org.springframework.http.ResponseEntity;

public class ResultMsg {
    String msg;
    ResponseEntity re;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseEntity getRe() {
        return re;
    }

    public void setRe(ResponseEntity re) {
        this.re = re;
    }

    public ResultMsg(String msg, ResponseEntity re) {
        this.msg = msg;
        this.re = re;
    }
}
