package com.techu.apitechudb.controllers;


import com.techu.apitechudb.ResultMsg;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    //Aqui pondriamos la parte REST
    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getProducts() {
        System.out.println("getProducts");
        //return this.PurchaseService.findAll(); //llama al findAll del PurchaseService
        return new ResponseEntity<>(this.purchaseService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/purchases/{id}")
    public ResponseEntity<PurchaseModel> getProductById(@PathVariable String id) {
        System.out.println("getProductById en ProductController");
        Optional<PurchaseModel> result = this.purchaseService.findById(id);

        //devolvemos un objeto vacio con un OK o NOT FOUND
        return new ResponseEntity<>(
                    result.isPresent() ? result.get() : new PurchaseModel(),
                    result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
                    );
    }

    //CREAR UNA COMPRA
    @PostMapping("/purchases")
    public ResultMsg addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase. ID: "+purchase.getId());
        System.out.println("addPurchase. userId: "+purchase.getUserId());

        String errorMsg = "";

        //Sanity Checks:
        //1. Check si el ID_USER para la compra existe ya en BBDD
        if (purchaseService.checkUser(purchase.getUserId())) {
            System.out.println("El idUser de la compra está en BBDD");

            //2. Check que no exista ya una compra con ese ID_COMPRA
            if (!purchaseService.checkPurchase(purchase.getId())){
                System.out.println("El idPurchase aun NO EXISTE en BBDD y se puede crear");

                //3. Check que todos los productos estan en BBDD ID_PRODUCTOS
                if (purchaseService.checkProducts(purchase.getPurchaseItems())){
                    System.out.println("Todos los PRODUCTOS EXISTEN en BBDD");

                    //a. Calculamos el AMOUNT de la compra
                    System.out.println("Guardamos en BBDD la COMPRA");
                    purchase.setAmount(purchaseService.getAmountPurchase());
                    //reestablecemos el amount
                    purchaseService.setAmountPurchase(0.F);
                    //b. Guardamos en BBDD
                    return new ResultMsg("CREATED",
                                        new ResponseEntity<>((this.purchaseService.addPurchase(purchase)), HttpStatus.CREATED)
                    );
                }
                else errorMsg="Alguno de los Productos NO Existe";
            }
            else errorMsg="El ID COMPRA ya existe";

        }
        else errorMsg="No existe el ID user";

        return new ResultMsg(errorMsg, new ResponseEntity<>(purchase, HttpStatus.NOT_FOUND));

    }

    @PutMapping("/purchases/{id}")
    public ResponseEntity<PurchaseModel> updateProduct(@RequestBody PurchaseModel product, @PathVariable String id) {
        System.out.println("updateProduct. ID: "+product.getId());

        //Optional<PurchaseModel> productToUpdate = this.PurchaseService.findById(id)
        //if (productToUpdate.isPresent()){
        //  this.PurchaseService.updateProduct(product);
        //}
        // return new ResponseEntity.....

        Optional<PurchaseModel> productUpdated = Optional.ofNullable(this.purchaseService.updatePurchase(product));

        return new ResponseEntity<>(product,
                productUpdated.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("purchases/{id}")
    public ResponseEntity<String> deletePurchase(@PathVariable String id) {
        System.out.println("deletePurchase. ID: "+id);
        boolean result = this.purchaseService.deletePurchase(id);

        return new ResponseEntity<>(
                result ? "COMPRA Borrada" : "Compra NO Borrada",
                result ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }


}

