package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    //Aqui pondriamos la parte REST
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name = "sort", required = false) String sort) {
        System.out.println("getUsers");

        if ("true".equals(sort)){
            System.out.println(("Sort presente: true"));
            return new ResponseEntity<>(this.userService.findAll(true), HttpStatus.OK);
        }
        else System.out.println(("Sort NO ESTA: false"));

        return new ResponseEntity<>(this.userService.findAll(false), HttpStatus.OK);
    }

    @GetMapping("/users/")
    public ResponseEntity<List<UserModel>> getUsers() {
        System.out.println("getUsers");
        return new ResponseEntity<>(this.userService.findAll(false), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<UserModel> getUserById(@PathVariable String id) {
        System.out.println("getUserById en UserController");
        Optional<UserModel> result = this.userService.findById(id);

        //devolvemos un objeto vacio con un OK o NOT FOUND
        return new ResponseEntity<>(
                    result.isPresent() ? result.get() : new UserModel(),
                    result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
                    );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser. ID: "+user.getId());
        return new ResponseEntity<>((this.userService.addUser(user)), HttpStatus.CREATED);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser. ID: "+user.getId());

        //Optional<UserModel> productToUpdate = this.userService.findById(id)
        //if (productToUpdate.isPresent()){
        //  this.userService.updateProduct(product);
        //}
        // return new ResponseEntity.....

        boolean userUpdated = this.userService.updateUser(user, id);
        if (!userUpdated)
            System.out.println(("NO EXISTE EL USUARIO"));
        return new ResponseEntity<>(user,
                userUpdated ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser (@PathVariable String id) {
        System.out.println("deleteUser. ID: "+id);
        boolean result = this.userService.deleteUser(id);

        return new ResponseEntity<>(
                result ? "Usuario Borrado" : "Usuario NO Borrado",
                result ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }


}

