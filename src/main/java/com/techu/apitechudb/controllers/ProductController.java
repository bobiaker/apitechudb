package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    //Aqui pondriamos la parte REST
    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");
        //return this.productService.findAll(); //llama al findAll del ProductService
        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/products/{id}")
    public ResponseEntity<ProductModel> getProductById(@PathVariable String id) {
        System.out.println("getProductById en ProductController");
        Optional<ProductModel> result = this.productService.findById(id);

        //devolvemos un objeto vacio con un OK o NOT FOUND
        return new ResponseEntity<>(
                    result.isPresent() ? result.get() : new ProductModel(),
                    result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
                    );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct. ID: "+product.getId());
        return new ResponseEntity<>((this.productService.addProduct(product)), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct. ID: "+product.getId());

        //Optional<ProductModel> productToUpdate = this.productService.findById(id)
        //if (productToUpdate.isPresent()){
        //  this.productService.updateProduct(product);
        //}
        // return new ResponseEntity.....

        Optional<ProductModel> productUpdated = Optional.ofNullable(this.productService.updateProduct(product));

        return new ResponseEntity<>(product,
                productUpdated.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct. ID: "+id);
        boolean result = this.productService.deleteProduct(id);

        return new ResponseEntity<>(
                result ? "Producto Borrado" : "Producto NO Borrado",
                result ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }


}

