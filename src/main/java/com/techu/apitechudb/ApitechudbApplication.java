package com.techu.apitechudb;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.*;

@SpringBootApplication
public class ApitechudbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);

	}

}
