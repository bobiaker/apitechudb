package com.techu.apitechudb.services;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {
    //Una clase SERVICIO utiliza una clase REpositorio que es la que realmente va a pedir al Modelo la info
    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;

    float amountPurchase = 0.F;

    public boolean checkUser (String userId)
    {
        Optional<UserModel> userModel = userService.findById(userId);
        if (userModel.isPresent()) return true;
        else {
            System.out.println("No existe el userID en BD");
            return false;
        }
    }

    public boolean checkPurchase (String idPurchase)
    {

        Optional<PurchaseModel> purchaseModel = this.findById(idPurchase);
        if (purchaseModel.isPresent()) return true;
        else {
            System.out.println("Ya existe la COMPRA en BD");
            return false;
        }
    }

    public boolean checkProducts (Map<String, Integer> purchaseItems)
    {
        //Iterator por los productos y consultar a BBDD para saber si existen
        boolean productNotFound = false;

        if (purchaseItems != null) {
            for (String idProduct : purchaseItems.keySet()) {
                Integer quantity = purchaseItems.get(idProduct);
                System.out.println(idProduct + " : " + quantity);
                //Check every idProduct
                Optional<ProductModel> productModel = productService.findById(idProduct);
                if (!productModel.isPresent()) {
                    productNotFound = true;
                    System.out.println("Product " + idProduct + " NOT FOUND in BBDD");
                }
                else
                {
                    //Aprovechamos para ir calculando el AMOUNT
                    ProductModel product = productModel.get();
                    //Cantidad * Precio
                    System.out.println("Amount Compra Actual: "+this.amountPurchase);
                    System.out.println("Amount Compra: (quantity: "+quantity+" * price:"+product.getPrice()+"): "+ (quantity * product.getPrice()));
                    this.amountPurchase = this.amountPurchase + (quantity * product.getPrice());
                    System.out.println("Amount Compra updated: "+this.amountPurchase);
                }
            }
        }

        return (productNotFound? false : true);

    }

    public float getAmountPurchase()
    {
        return this.amountPurchase;
    }

    public void setAmountPurchase(float amount)
    {
        this.amountPurchase = amount;
    }

    public List<PurchaseModel> findAll() {
        System.out.println("findAll en PurchaseService");
        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById en PurchaseService");
        return this.purchaseRepository.findById(id);
    }

    public PurchaseModel addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase en PurchaseService");
        return this.purchaseRepository.save(purchase);
    }

    public PurchaseModel updatePurchase(PurchaseModel purchase) {
        System.out.println("updatePurchase en PurchaseService");
        //El Save hace update si lo encuentra, sino lo inserta
        return this.purchaseRepository.save(purchase);
    }

    public boolean deletePurchase(String id)
    {
        System.out.println("deletePurchase en PurchaseService");
        boolean isDelete = false;

        if (this.findById(id).isPresent()) {
            this.purchaseRepository.deleteById(id);
            isDelete = true;
        }

        return isDelete;
    }
}
