package com.techu.apitechudb.services;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    //Una clase SERVICIO utiliza una clase REpositorio que es la que realmente va a pedir al Modelo la info
    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll en ProductServices");
        return this.productRepository.findAll();
    }

    //Optional: Es de Java8. Patron de Diseño para evitar los NullPointerExceptions.
    //encapsulado en esta clase que permite devolver siempre como minimo un objeto "Optional"
    //aunque el "ProductModel" sea NULL
    //ademas tiene metodos para indicar si contiene el Objeto que va entre <> o no: .isPresent()
    //Pensado para SER UN TIPO DE RETORNO
    public Optional<ProductModel> findById(String id) {
        System.out.println("findById en ProductServices");
        return this.productRepository.findById(id);

    }

    public ProductModel addProduct(ProductModel product) {
        System.out.println("addProduct en ProductService");
        return this.productRepository.save(product);
    }

    public ProductModel updateProduct(ProductModel product) {
        System.out.println("updateProduct en ProductService");
        //El Save hace update si lo encuentra, sino lo inserta
        return this.productRepository.save(product);
    }

    public boolean deleteProduct(String id)
    {
        System.out.println("deleteProduct en ProductService");
        boolean isDelete = false;

        if (this.findById(id).isPresent()) {
            this.productRepository.deleteById(id);
            isDelete = true;
        }

        return isDelete;
    }
}
