package com.techu.apitechudb.services;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    //Una clase SERVICIO utiliza una clase REpositorio que es la que realmente va a pedir al Modelo la info
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(boolean sortByAge) {
        System.out.println("findAll en UserService. sortByAge: "+sortByAge);
        if (sortByAge)
            return this.userRepository.findAll(Sort.by(Sort.Direction.DESC, "age"));
        else
            return this.userRepository.findAll();
    }

    //Optional: Es de Java8. Patron de Diseño para evitar los NullPointerExceptions.
    //encapsulado en esta clase que permite devolver siempre como minimo un objeto "Optional"
    //aunque el "UserModel" sea NULL
    //ademas tiene metodos para indicar si contiene el Objeto que va entre <> o no: .isPresent()
    //Pensado para SER UN TIPO DE RETORNO
    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserServices");
        return this.userRepository.findById(id);

    }

    public UserModel addUser(UserModel user) {
        System.out.println("addUser en UserServices");
        return this.userRepository.save(user);
    }

    public boolean updateUser(UserModel user, String id) {
        System.out.println("updateUser en UserServices. User ID: "+id);
        boolean isUpdated = false;

        if (this.findById(id).isPresent()) {
            //El ID no se puede modificar, si la info del Objeto no coincide con el ID de la URL no se actualiza
            if (user.getId().equals(id)) {
                System.out.println(("Usuario encontrado y actualizado"));
                this.userRepository.save(user);
                isUpdated = true;
            }
        }

        return isUpdated;

    }

    public boolean deleteUser(String id)
    {
        System.out.println("deleteUser en UserServices");
        boolean isDelete = false;

        if (this.findById(id).isPresent()) {
            this.userRepository.deleteById(id);
            isDelete = true;
        }

        return isDelete;
    }
}
