package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {
    //Las interfaces en Java permiten un comportamiento Horizontal. Extender un comportamiento, sin importar
    //si las clases que lo extienden son homogeneas o parecidas

    //Le indicamos que el repositorio es de tipo Mongo, y que guarda ProductModel y la clave es un String(ID)
    //public List<ProductModel> findAllFromArray() {
    //    return ApitechudbApplication.productModels;
    //}
}
