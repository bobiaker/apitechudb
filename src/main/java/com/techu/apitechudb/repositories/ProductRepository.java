package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends MongoRepository<ProductModel, String> {
    //Las interfaces en Java permiten un comportamiento Horizontal. Extender un comportamiento, sin importar
    //si las clases que lo extienden son homogeneas o parecidas

    //Le indicamos que el repositorio es de tipo Mongo, y que guarda ProductModel y la clave es un String(ID)
    //public List<ProductModel> findAllFromArray() {
    //    return ApitechudbApplication.productModels;
    //}
}
